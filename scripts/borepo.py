#!/usr/bin/env python3

import logging
import hashrepo

log = logging.getLogger('hashlib')
try: 
    logging.basicConfig(format='%(levelname)s:%(asctime)s %(message)s', 
            filename='/var/log/hashlib.log', 
            filemode='a', level=logging.DEBUG)
except PermissionError:
    pass
log.debug('\n\n')
logconsolehandler=logging.StreamHandler(sys.stdout)
logconsolehandler.setLevel(logging.INFO)
log.addHandler(logconsolehandler)

