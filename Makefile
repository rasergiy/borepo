
all:
	echo "Nothing to do"

clean:
	-rm -rf MANIFEST build dist __pycache__ */__pycache__ */*/__pycache__ *.pyc */*.pyc */*/*.pyc

install_scripts:
	cp scripts/borepo.py /usr/bin/borepo
	chmod 0755 /usr/bin/borepo

