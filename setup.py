import os
from distutils.core import setup


def read(fname):
    return open(os.path.join(os.path.dirname(__file__), fname)).read()

setup(
        name='borepo',
        author='Sergei Raskin',
        author_email='rasergiy@gmail.com',
        url='https://bitbucket.org/rasergiy/borepo',
        long_description=read('README'),
        version='0.1',
        packages = ['borepo'],
)
