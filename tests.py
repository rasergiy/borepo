import re, os, os.path, time
import sys
import hashlib
import shutil
import string
import random
import logging
import unittest
import multiprocessing
import borepo
import xml.etree
from borepo import *
from borepo.const import *
import borepo.errors as errors


LOCKTIMEOUT       = 0.2
LOCKTIMEOUT_DELTA = 0.2
DELETE_OBJECTS    = False

HASHTEXT = (b'Hashed text md5', '629d1fc7c50c20ab8c054425eee044f0')

TEXT = 'Test text: <escaped> ~`!@#$%^&*()-=+\|\/?.,<<<>>>"\'text\n\n'

BINTEXT = bytes(TEXT, 'utf-8')

META_CHECK = (
        ('meta1', 'meta1 &nbsp; ><<>>! <!-- value -->'),
        ('meta2', 'meta2 value'),
        ('meta3', 'meta3 value'),
        ('meta4', TEXT))

CACHE_CHECK = (
        ('001', b'\x00\x01\x00\x02\x00\x00\x00\x03'),
        ('002', b'<Text>'))

CACHE_CHECK_STR = (
        ('003', 'String cache'),
        ('004', ''))

BASEPATH = '/tmp/borepo-test'

log = logging.getLogger('borepo')
log.level = logging.INFO
logconsolehandler=logging.StreamHandler(sys.stdout)
logconsolehandler.setLevel(logging.DEBUG)
log.addHandler(logconsolehandler)

def random_string(n=8):
    return ''.join([random.choice(string.ascii_letters)] + 
            [random.choice(a) for i in range(n-1)])

def clean_obj(Cls, *args, **kwargs):
    obj = Cls(*args, **kwargs)
    if os.path.exists(obj.basepath):
        shutil.rmtree(obj.basepath)
    return obj


class TestBORFile(unittest.TestCase):
    
    def assertLockedByAnotherProcess(self, Cls, *args, **kwargs):
        obj = Cls(*args, **kwargs)
        self.assertTrue(obj.locked)
        self.assertTrue(obj.locked != str(os.getpid()))
        try:
            obj.unlock()
            self.fail('Object locked by another process were unlocked: %s' % str(obj))
        except errors.BORUnlockError:
            pass
    
    def assertLockTimeout(self, Cls, *args, **kwargs):
        obj = Cls(*args, **kwargs)
        try:
            obj.lock(timeout=LOCKTIMEOUT)
            self.fail('Object locked before BORTimeOutError is raised: %s' % str(obj))
        except errors.BORTimeoutError:
            pass
    
    def assertFileContent(self, filepath, content):
        with open(filepath, 'br') as f:
            assert f.read()==content

    def test_BORFile(self, Cls=BORFile, *args, **kwargs):
        ''' Test class BORFile '''
        delete_object = kwargs.pop('delete_object', DELETE_OBJECTS)
        kwargs['basepath'] = kwargs.pop('basepath',  
                os.path.join(BASEPATH, 'file1'))
        obj = clean_obj(Cls, **kwargs)
        self.assertFalse(os.path.exists(obj.filepath))
        self.assertFalse(obj.exists())
        # --- Creation
        obj.create()
        self.assertTrue(obj.exists())
        self.assertTrue(os.path.exists(obj.filepath))
        self.assertTrue(Cls(**kwargs).exists())
        # --- Re-creation
        try:
            Cls(**kwargs).create()
            self.fail('Object was recreated, while it already exists')
        except errors.BORFileExistsError:
            pass
        # Lock
        try:
            obj.unlock()
            self.fail('Object was unlocked, while it is not locked')
        except errors.BORUnlockError:
            pass
        obj.lock()
        self.assertTrue(obj.locked)
        p = multiprocessing.Process(
                target=self.assertLockedByAnotherProcess, 
                args=(Cls,), kwargs=kwargs)
        p.start()
        p.join()
        obj.unlock()
        self.assertFalse(obj.locked)
        # --- Lock timeout
        block = Cls(**kwargs)
        block.lock()
        p = multiprocessing.Process(
                target=self.assertLockTimeout, 
                args=(Cls,), kwargs=kwargs)
        p.start()
        p.join()
        block.unlock()

        # --- Delete
        if delete_object:
            obj.delete()
            self.assertFalse(Cls(**kwargs).exists())
            self.assertFalse(os.path.exists(obj.filepath))
            self.assertFalse(obj.exists())
        return obj

    def test_BORBinFile(self, Cls=BORBinFile, *args, **kwargs):
        ''' Test class BORFile -> BORBinFile '''
        kwargs['basepath'] = kwargs.pop('basepath',  
                os.path.join(BASEPATH, 'binfile1'))
        delete_object = kwargs.pop('delete_object', DELETE_OBJECTS)
        kwargs['delete_object'] = False
        obj = self.test_BORFile(BORBinFile, *args, **kwargs)
        with obj as a:
            with a as b: 
                b.data = HASHTEXT[0]
        self.assertTrue(Cls(**kwargs).read()==HASHTEXT[0])
        self.assertEqual(Cls(**kwargs).get_hash('md5').hexdigest(), HASHTEXT[1])
        self.assertFileContent(Cls(**kwargs).filepath, HASHTEXT[0])
        # --- Delete
        if delete_object:
            obj.delete()
            self.assertFalse(Cls(**kwargs).exists())
            self.assertFalse(os.path.exists(obj.filepath))
            self.assertFalse(obj.exists())
        return obj
    
    def test_BORXmlFile(self, Cls=BORXmlFile, *args, **kwargs):
        ''' Test class BORFile -> BORXmlFile '''
        xml_test_len = 30
        kwargs['basepath'] = kwargs.pop('basepath',  
                os.path.join(BASEPATH, 'xmlfile1'))
        delete_object = kwargs.pop('delete_object', DELETE_OBJECTS)
        kwargs['delete_object'] = False
        obj = self.test_BORFile(Cls, *args, **kwargs)

        self.assertEqual(obj.version, borepo.VERSION)
        for i in range(xml_test_len):
            obj.add_element('element-a%s' % i, 'testa%s' % i)
            obj.add_element('element-b%s' % i)
            obj.set_element('element-b%s' % i, 'testb%s' % i)
            obj.add_element('nodelem%s' % i, count='0')
            obj.set_node('nodelem%s' % i, 'node1', '1')
            obj.set_node('nodelem%s' % i, 'node2', '2')
            obj.set_node('nodelem%s' % i, 'node2', '2')
        for i in range(xml_test_len):
            self.assertEqual('testa%s' % i,
                    Cls(*args, **kwargs).get_element('element-a%s' % i).text)
            self.assertEqual('testb%s' % i,
                    Cls(*args, **kwargs).get_element('element-b%s' % i).text)
            self.assertEqual(2,
                    Cls(*args, **kwargs).get_element_count('nodelem%s' % i))
            self.assertEqual('1',
                    Cls(*args, **kwargs).get_node('nodelem%s' % i, 'node1').text)
            self.assertEqual('2',
                    Cls(*args, **kwargs).get_node('nodelem%s' % i, 'node2').text)
        for i in range(xml_test_len):
            if i%3==0:
                obj.set_node('nodelem%s' % i, 'node1', None)
                obj.remove_element('element-a%s' % i)
            elif i%3==1:
                obj.remove_node('nodelem%s' % i, 'node1')
        for i in range(xml_test_len):
            if i%3==0:
                self.assertEqual(None, obj.get_node('nodelem%s' % i, 'node1'))
                try:
                    elem = obj.get_element('element-a%s' % i)
                    self.fail('Removed element received')
                except errors.BORXmlElementNotFoundError:
                    pass
            elif i%3==1:
                self.assertEqual(None, obj.get_node('nodelem%s' % i, 'node1'))
            else:
                self.assertNotEqual(None, obj.get_node('nodelem%s' % i, 'node1'))


        # --- Delete
        if delete_object:
            obj.delete()
            self.assertFalse(Cls(**kwargs).exists())
            self.assertFalse(os.path.exists(obj.filepath))
            self.assertFalse(obj.exists())
        return obj
    """
    def test_BORSqlite3File(self, Cls=BORSqlite3File, *args, **kwargs):
        ''' Test class BORFile -> BORSqlite3File '''
        kwargs['basepath'] = kwargs.pop('basepath',  
                os.path.join(BASEPATH, 'sqlfile1'))
        delete_object = kwargs.pop('delete_object', DELETE_OBJECTS)
        
        obj = clean_obj(Cls, **kwargs)
        self.assertFalse(os.path.exists(obj.filepath))
        self.assertFalse(obj.exists())
        obj.create()
        with obj:
            obj.flush()
            row = obj.get_single_row('settings', 'name="%s"' % "count") 

        # --- Delete
        if delete_object:
            obj.delete()
            self.assertFalse(Cls(**kwargs).exists())
            self.assertFalse(os.path.exists(obj.filepath))
            self.assertFalse(obj.exists())
        return obj
        """

    def test_BORObjectIndex(self, Cls=BORObjectIndex, *args, **kwargs):
        ''' Test class BORFile -> BORObjectIndex '''
        xml_test_len = 30
        kwargs['basepath'] = kwargs.pop('basepath',  
                os.path.join(BASEPATH, 'objectindex'))
        delete_object = kwargs.pop('delete_object', DELETE_OBJECTS)
        kwargs['delete_object'] = False
        obj = self.test_BORFile(Cls, *args, **kwargs)
        # self.assertEqual(obj.version, borepo.VERSION)
        
        obj.cache.set('cacheitem')
        
        d = hashlib.new('md5')
        d.update(HASHTEXT[0])
        obj.hash.set('md5', d.hexdigest())
        
        for i in range(xml_test_len):
            obj.cache.set('cacheitem%s' % i)
        for i in range(xml_test_len):
            if i%2:
                obj.cache.set('cacheitem%s' % i, None)

        
        t = Cls(*args, **kwargs)

        self.assertEqual(t.hash.get('md5'), HASHTEXT[1])

        t.ensure_load()
        for i in range(xml_test_len):
            if i%2:
                self.assertEqual(False, t.cache('cacheitem%s' % i))
            else:
                self.assertEqual(True, t.cache('cacheitem%s' % i))

        # --- Delete
        if delete_object:
            obj.delete()
            self.assertFalse(Cls(**kwargs).exists())
            self.assertFalse(os.path.exists(obj.filepath))
            self.assertFalse(obj.exists())
        return obj
    
    """
    def test_BORepo(self, Cls=BORepo, *args, **kwargs):
        ''' Test class BORFile -> BORepo '''
        kwargs['basepath'] = kwargs.pop('basepath',  
                os.path.join(BASEPATH, 'borindex'))
        delete_object = kwargs.pop('delete_object', DELETE_OBJECTS)
        
        obj = clean_obj(Cls, **kwargs)
        self.assertFalse(os.path.exists(obj.filepath))
        self.assertFalse(obj.exists())
        obj.create()
        with obj:
            obj.flush()
            row = obj.get_single_row('settings', 'name="%s"' % "count") 

        # --- Delete
        if delete_object:
            obj.delete()
            self.assertFalse(Cls(**kwargs).exists())
            self.assertFalse(os.path.exists(obj.filepath))
            self.assertFalse(obj.exists())
        return obj
    """

if os.path.exists(BASEPATH):
    shutil.rmtree(BASEPATH)
os.mkdir(BASEPATH)

if __name__ == '__main__':
    unittest.main()
