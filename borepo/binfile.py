import os
import hashlib
from borepo.const import *
from borepo.file import BORFile

class BORBinFile(BORFile):

    __filename__  = 'data.bin'
    __file_mode__ = 'br+'
    __data__ = None
    __mode__ = None
    __file__ = None
    
    def __init__(self, **kwargs):
        self.__file_mode__ = kwargs.pop('file_mode', 
                self.__file_mode__)
        super(BORBinFile, self).__init__(**kwargs)

    def __enter__(self):
        return super(BORBinFile, self).__enter__()

    def __exit__(self, exception_type=None, 
                exception_value=None, traceback=None):
        return super(BORBinFile, self).__exit__(exception_type=exception_type,
                exception_value=exception_value, traceback=traceback)
    
    def __open__(self, mode='br+'):
        ''' Open associated file '''
        self.__mode__ = mode
        self.__file__ = open(self.filepath, mode)
    
    def __close__(self, *args, **kwargs):
        ''' Close associated file '''
        self.__file__.close()
        self.__file__ = None
        self.__mode__ = None
    
    def read(self):
        ''' -> entire file's data
        If object is not locked - it will be locked and unlocked while reading
        '''
        self.lock()
        self.__data__ = self.__file__.read()
        self.set_changed(False)
        self.unlock()
        return self.__data__
    
    def flush(self):
        ''' Flush data to the file ''' 
        self.__file__.write(self.data)
        self.set_changed(False)
    
    def ensure_load(self):
        ''' Make sure data is loaded '''
        if self.__data__ is None:
            self.read()
    
    def get_data(self):
        ''' -> data '''
        self.ensure_load()
        return self.__data__

    def set_data(self, data):
        ''' set new data '''
        self.__data__ = data
        self.set_changed(True)

    def get_hash(self, algorithm):
        ''' -> hash of file. '''
        self.ensure_load()
        dig = hashlib.new(algorithm)
        dig.update(self.__data__)
        return dig

    def create(self, data_src=bytes(), on_data=None):
        ''' create object from data (bytes, io.IOBase, ChunkedFile) '''
        super(BORBinFile, self).create()
        if isinstance(data_src, bytes):
            self.__data__=data_src
            self.__open__(mode='wb+')
            self.flush()
            self.__close__()
            if on_data:
                on_data(self.__data__)
        else:
            with ChunkedFile.get(data_src, 'rb') as src:
                with ChunkedFile.get(self.filepath, 'wb+') as dst:
                    for chunk in src.chunks():
                        chunk = chunk.encode()
                        dst.write(chunk)
                        if on_data:
                            on_data(chunk)
        self.set_changed(False)
        return self
    
    def delete(self):
        ''' Deletes data file '''
        self.assert_delete()
        self.__data__ = None
        os.remove(self.filepath)
        super(BORBinFile, self).delete()
    
    hash     = property(get_hash)
    data     = property(get_data, set_data)
