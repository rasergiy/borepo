''' 
hashrepo - Hashed Objects Repository

This module provides standart object's repository mechanism
Object - standartized objects with strict format

BORFile -> BORBinFile
        -> BORXmlFile
        -> BORSqlite

create - return self, but doesnt locks!
'''

import borepo.errors as errors
from borepo.file import BORFile
from borepo.binfile import BORBinFile
from borepo.xmlfile import BORXmlFile
from borepo.sqlite3file import BORSqlite3File
from borepo.borepo import BORepo
from borepo.object import BORObject
from borepo.objectindex import BORObjectIndex

