import os
import copy
import hashlib
from borepo.const import *
from borepo.sqlite3file import BORSqlite3File

__all__ = [
        'BORepo']

class BORepo(BORSqlite3File):
    
    ''' 
    BOR Index
    Contains meta data about repository or repository's objects
    '''

    __filename__ = 'index.db'
    __timestamp_format__ = '%Y%m%d%H%M%S'
    # hash table will be generated dynamically
    __tables__ = [('settings', 
            ('name', 'CHAR(100)'), 
            ('value', 'TEXT')),]
    __initial__ = (('settings', 
            ('hash_algorithm', DEFAULT_HASH_ALGORITHM), 
            ('count', 0)),)
        
    __hash_algorithm__ = None
    __enter__ = BORSqlite3File.__enter__
    __exit__  = BORSqlite3File.__exit__

    def __init__(self, *args, **kwargs):
        tbl = ['hash', ] + [(alg, 'CHAR(%s)' % (hashlib.new(alg).digest_size*2)) \
                for alg in HASH_ALGORITHMS]
        self.__tables__ = copy.deepcopy(self.__tables__)
        self.__tables__.append(tbl)
        super(BORepo, self).__init__(*args, **kwargs)
        self.__objects_path__ = os.path.join(
                self.basepath, 'data')
        self.__temp_path__ = os.path.join(
                self.basepath, 'temp')
    
    def __str__(self):
        return 'BORepo %s/ Lock:%s' % (self.basepath, self.locked)

    def __len__(self):
        return self.get_setting('count', int)

    def create(self, *args, **kwargs):
        super().create(*args, **kwargs)
        os.makedirs(self.objects_path)
        os.makedirs(self.temp_path)
    
    def push_object(self, data_src):
        ''' -> temporary created object from data_src '''
        path = os.path.join(self.temp_path, '%s-%s' % (
                time.strftime(self.__timestamp_format__, time.localtime()),
                ''.join(random.choice(string.ascii_letters) for i in range(4))))
        return BORObject(basepath=path).create(data_src)
    
    def add_object(self, obj, move=False):
        ''' -> Object, Add object to the repository  '''
        if not move:
            raise NotImplementedError()
        hsh = obj.get_hash(self.hash_algorithm)
        path = self.get_object_path(hsh)
        t = BORObject(basepath = path)
        if t.exists():
            return t
        self.pushlock()
        obj.move(path)
        hashes = {alg:obj.get_hash(alg) for alg in HASH_ALGORITHMS}
        self.add_single_row('hash', **hashes)
        self.set_count(self.count+1)
        self.poplock()
        return obj

    def has(self, obj_or_hash):
        if isinstance(obj_or_hash, BORObject):
            hsh = obj_or_hash.get_hash(self.hash_algorithm)
        else:
            hsh = obj_or_hash
        return os.path.exists(self.get_object_path(hsh))
    
    def get_object_path(self, hsh):
        ''' -> Object path '''
        return os.path.join(self.objects_path, hsh[:2], hsh[2:4], hsh[4:6], hsh[6:])
    
    def get_temp_path(self):
        return self.__temp_path__
    
    def get_objects_path(self):
        return self.__objects_path__
    
    def get(self, hsh):
        path = self.get_object_path(hsh)
        if os.path.exists(path):
            return BORObject(basepath=path)
        return False
    
    def get_setting(self, name, cls=None):
        return self.get_single_row('settings', 
                'name="%s"' % name, column=name, column_cls=cls)
    
    def set_setting(self, name, value):
        self.set_single_row('settings', 
                'name="%s"' % name, name=name, value=value)

    def get_hash(self, hsh):
        alg = self.get_setting('hash_algorithm')
        return self.get_single_row('hash', '%s="%s"' % (alg, hsh))

    def get_hash_algorithm(self):
        if self.__hash_algorithm__ is None:
            self.__hash_algorithm__ = self.get_setting('hash_algorithm')
        return self.__hash_algorithm__

    def get_count(self):
        return self.get_setting('count', int)
    
    def set_count(self, count):
        return self.set_setting('count', count)
    
    hash_algorithm  = property(get_hash_algorithm)
    count           = property(get_count)
    temp_path       = property(get_temp_path)
    objects_path    = property(get_objects_path)

