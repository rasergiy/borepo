import logging

log = logging.getLogger('borepoerrors')


class BORError(BaseException):
    ''' BOR Basic exception '''
    def __init__(self, msg):
        super(BORError, self).__init__(msg)
        #log.error('%s %s' %(str(self.__class__), str(self)))

class BORTimeoutError(BORError):
    ''' BOR FIle already exists error '''
    def __init__(self, t):
        super(BORTimeoutError, self).__init__(
                'Timeout: %s' % t)

class BORFileExistsError(BORError):
    ''' BOR FIle already exists error '''
    def __init__(self, file):
        super(BORFileExistsError, self).__init__(
                'BOR File already exists: %s' % file)

class BORLockError(BORError):
    ''' BOR lock error '''
    def __init__(self, file):
        super(BORLockError, self).__init__(
                'Can\'t lock: %s' % file)

class BORUnlockError(BORError):
    ''' BOR FIle already exists error '''
    def __init__(self, file, msg=''):
        super(BORUnlockError, self).__init__(
            'Can\'t unlock: %s (%s)' % (file, msg))

class BORXmlError(BORError):
    ''' BOR Xml base error '''

class BORXmlElementNotFoundError(BORXmlError):
    ''' BOR Xml Element  '''
    def __init__(self, elem_name):
        super(BORXmlElementNotFoundError, self).__init__(
                'BOR Xml Element Not Found: %s' % elem_name)

class BORXmlElementNameError(BORXmlError):
    ''' BOR Xml Element name is wrong  '''
    def __init__(self, elem_name):
        super(BORXmlElementNameError, self).__init__(
                'BOR Xml Element wrong name: %s' % elem_name)

class BORNameError(BORError):
    ''' BOR name is wrong  '''
    def __init__(self, name):
        super(BORNameError, self).__init__(
                'BOR Name wrong: %s' % name)

class BORMultipleRowsSelectedSqlError(BORError):
    ''' BOR sql multiple rows '''
    def __init__(self, cnt, q):
        super(BORNameError, self).__init__(
                'BOR Multiple Rows (%s) Selected, when one is expected: %s' % (
                        cnt, q))

class BORFileDeleteError(BORError):
    ''' BOR name is wrong  '''
    def __init__(self, file, reason):
        super(BORFileDeleteError, self).__init__(
                'Cannot delete file %s: %s' % (file, reason))

