'''
BOR Object Index
'''

from borepo.const import *
from borepo.sqlite3file import *

__all__ = ['BORObjectIndex']


class SQLCache(SQLTable):

    def __init__(self):
        super(SQLCache, self).__init__('cache', 
                ('name', 'CHAR(255)'))
    
    def __call__(self, name, value=None):
        if value is None:
            return self.get(name)
        else:
            self.set(name, value)

    def get(self, name):
        return bool(self.get_single_row('name="%s"' % name))

    def set(self, name, value=True):
        return super(SQLCache, self).set(name, value)

class BORObjectIndex(BORSqlite3File):

    ''' 
    BOR Object Index
    Contains data object description, meta and cach information
    '''

    __filename__ = 'object.db'

    hash = SQLHash('hash')
    prop = SQLDict('prop', 'CHAR(64)',  'CHAR(64)', 
            hash_algorithm=DEFAULT_HASH_ALGORITHM,
            version=VERSION)
    cache = SQLCache()


