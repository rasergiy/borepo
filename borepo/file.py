'''
Binary Object Repository file
TODO: Avoid django usage
'''

import os
import time
import fcntl
import hashlib
import logging
import borepo.errors as errors
from borepo.const import *
from django.core.files.base import File

__all__ = ['BORFile', 'ChunkedFile']

log = logging.getLogger('borepo')

class ChunkedFile(File):

    ''' Django's Chunked file. '''

    default_mode = 'rb'

    def __init__(self, src, mode=default_mode):
        if isinstance(src, str):
            return super(ChunkedFile, self).__init__(open(src, mode))
        return super(ChunkedFile, self).__init__(src)

    @classmethod
    def get(cls, src, mode=default_mode):
        ''' -> ChunkedFile instance, created form the src.
            valid source: ChunkdFile, file'''
        if not isinstance(src, cls):
            return cls(src, mode)
        return src

class BORFile:

    ''' Basic BOR File object '''

    __pid__ = None
    __filename__ = ''
    __basepath__ = None
    __unlockfreq__ = 0.001
    __unlock_timeout__ = 1
    
    def __init__(self, **kwargs):
        self.__unlockfreq__ = kwargs.pop('unlockfreq', 
                self.__unlockfreq__)
        self.__basepath__ = kwargs.get('basepath', 
                self.__basepath__)
        self.__filename__ = kwargs.get('filename', 
                self.__filename__)
        self.__locked__ = None
        self.__lockbuf__ = list()
        self.__pid__ = str(os.getpid())
        self.set_changed(False)

    def __str__(self):
        return '%s: %s (Lock:%s)' % (self.__class__.__name__, 
                self.filepath, self.locked)

    def __not_implemented__(self, *args, **kwargs):
        raise NotImplementedError()
    
    def create(self):
        '''
        Creates directory structure, raises BORFileExistsError
        if path exists
        '''
        if self.exists():
            raise errors.BORFileExistsError(self.filepath)
        if not os.path.exists(self.basepath):
            os.makedirs(self.basepath)
        return self

    def exists(self):
        ''' -> bool '''
        return os.path.exists(self.filepath)

    def assert_delete(self):
        ''' Assert deletion possibility '''
        if self.locked==self.pid:
            raise errors.BORFileDeleteError(self.filepath, 'file is locked')
        if self.locked:
            self.unlock_wait()
    
    def delete(self):
        ''' Deletes data file '''
        self.assert_delete()
        os.rmdir(self.basepath)
        self.set_changed(False)

    def lock(self, *args, **kwargs):
        ''' Lock object '''
        lock = (self.locked!=self.pid)
        if lock:
            self.unlock_wait(timeout=kwargs.pop('timeout', None))
            with open(self.lockpath, 'w+') as lockfile:
                lockfile.write(self.pid)
            self.__locktime__ = time.time()
            self.__open__(*args, **kwargs)
            self.__locked__ = True
        self.__lockbuf__.append(lock)
        return self
    
    def unlock(self, **kwargs):
        if not self.__lockbuf__:
            raise errors.BORUnlockError(self, 
                    'Empty lock buffer')
        if self.__lockbuf__.pop():
            lp = self.get_lockpid() 
            if lp and lp != self.pid:
                raise errors.BORUnlockError(self, 
                        'Locked by process: %s' % lp)
            if self.changed:
                self.flush()
            os.remove(self.lockpath)
            self.__locked__ = None
            self.__close__()
    
    def unlock_wait(self, timeout=None):
        ''' 0 timeout = wait until unlocked '''
        timeout = timeout or self.__unlock_timeout__
        waitime = time.time()
        while self.locked:
            time.sleep(self.__unlockfreq__)
            if timeout is 0:
                next
            if time.time()-waitime > timeout:
                raise errors.BORTimeoutError(timeout)
        return time.time()-waitime

    def get_basepath(self, basepath=None):
        ''' -> string '''
        return basepath or self.__basepath__

    def get_filename(self, filename=None):
        ''' -> string '''
        return filename or self.__filename__
    
    def get_filepath(self, basepath=None, filename=None):
        ''' -> string '''
        return os.path.join(self.get_basepath(basepath),
                self.get_filename(filename))
    
    def get_lockpath(self):
        ''' -> string '''
        return self.filepath + '.lock'

    def get_lockpid(self):
        try:
            with open(self.lockpath, 'r') as f:
                pid = str(f.read()).strip()
        except FileNotFoundError:
            return None
        return pid
    
    def get_locked(self):
        ''' -> lock process pid or None '''
        if self.__locked__ is None:
            return self.get_lockpid()
        return self.pid
    
    def set_changed(self, value=True):
        self.__changed__ = value
        if self.__changed__:
            self.lock()
            self.flush()
            self.unlock()

    def get_pid(self):
        return self.__pid__

    def get_changed(self):
        return self.__changed__ 
    
    pid      = property(get_pid)
    locked   = property(get_locked)
    lockpid  = property(get_lockpid)
    changed  = property(get_changed)
    lockpath = property(get_lockpath)
    basepath = property(get_basepath)
    filename = property(get_filename)
    filepath = property(get_filepath)

    __enter__   = lock
    __exit__    = unlock
    __open__    = \
    __close__   = lambda x:x
    flush       = \
    ensure_load = __not_implemented__
