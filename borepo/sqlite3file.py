import os
import sys
import logging
import hashlib
import sqlite3
from borepo.const import *
import borepo.errors as errors
from borepo.file import BORFile

#__all__ = ['BORSqlite3File']

log = logging.getLogger('borepo')

class SQLTable:

    __interface__ = None
    __initial__ = dict()
    __id_proto__ = 'INTEGER PRIMARY KEY AUTOINCREMENT'

    def __init__(self, table_name, *columns):
        self.__name__ = table_name
        self.__proto__ = {col[0]:col[1] for col in columns}
        self.__proto__['id'] = self.__id_proto__
        self.__columns_names__ = ['id', ] + list(map(lambda x:x[0], columns))
        self.__columns_protos__ = list(self.__proto__[c] for c in self.__columns_names__)
    
    def create(self):
        with self.__interface__ as q:
            q.query('CREATE TABLE %s (%s)' % (
                    self.__name__, ', '.join('%s %s' % (k, v) \
                            for k,v in self.__proto__.items())))
    
    def maprow(self, row):
        return {name:row[i] for i,name in 
                enumerate(self.__columns_names__)}
    
    def get_single_row(self, where, column=None, column_cls=None):
        with self.__interface__ as q:
            s = 'SELECT %s FROM %s WHERE %s' % (
                    ','.join(self.__columns_names__), self.__name__, where)
            c = q.query(s)
            rows = list(c.fetchall())
            if len(rows)>1:
                raise errors.BORMultipleRowsSelectedSqlError(len(rows), q)
            elif len(rows)==1:
                ret = self.maprow(rows[0])
                if column:
                    ret = ret['column']
                    if column_cls:
                        ret = column_cls(ret)
            else:
                ret = None
        return ret
    
    def set_single_row(self,  where, **values):
        row = self.get_single_row(where)
        kw = {   'table': self.__name__,
                'rows':  ','.join(sorted(values.keys())),
                'rows_values': ', '.join('%s="%s"' % (k, values[k]) for k in sorted(values.keys())),
                'values': '"%s"' % '","'.join(values[k] for k in sorted(values.keys()))}
        if row is None:
            q = 'INSERT INTO %(table)s (%(rows)s) VALUES(%(values)s)' % kw
        else:
            kw['id'] = row['id']
            q = 'UPDATE %(table)s SET %(rows_values)s WHERE id=%(id)s' % kw
        with self.__interface__ as qint:
            qint.query(q)

    def add_single_row(self, table, **values):
        print(self.__interface__)
        k = {   'table': table,
                'rows':  ','.join(sorted(values.keys())),
                'values': '"%s"' % '","'.join(values[k] for k in sorted(values.keys()))}
        with self.__interface__ as qint:
            return qint.query('INSERT INTO %(table)s (%(rows)s) VALUES(%(values)s)' % k)

class SQLDict(SQLTable):

    def __init__(self, table, name, value, **initial):
        super(SQLDict, self).__init__(table, 
                ('name', name),
                ('value', value))
        self.name  = name
        self.value = value
        self.__initial__ = initial

    def create(self):
        super(SQLDict, self).create()
        for k,v in self.__initial__.items():
            self.add_single_row(self.__name__, name=k, value=v)

class SQLBool(SQLTable):

    def __init__(self, table_name):
        super(SQLBool, self).__init__(table_name, 
                ('name', 'CHAR(255)'))

    def __call__(self, name, value=None):
        if value is None:
            return self.get(name)
        else:
            self.set(name, value)
    
    def get(self, name):
        return bool(self.get_single_row('name="%s"' % name))

    def set(self, name, value=True):
        o = self.get_single_row('name="%s"' % name)
        if value and o is None:
            self.__interface__.query(
                'INSERT INTO %s (name) VALUES ("%s")' %(
                    self.__name__, name))
        elif not value and o:
            self.__interface__.query(
                'DELETE FROM %s WHERE id="%s"' %(
                    self.__name__, o['id']))

    
class SQLHash(SQLTable):

    def __init__(self, table_name):
        super(SQLHash, self).__init__(table_name, 
                ('name', 'CHAR(16)'),('value', 'CHAR(256)'))
        # t = [(alg, 'CHAR(%s)' % (hashlib.new(alg).digest_size*2)) \
        #            for alg in HASH_ALGORITHMS]
        #super(SQLHash, self).__init__(table, *t)
    
    def get(self, name):
        row = self.get_single_row('name="%s"' % name)
        if row:
            return row['value']
        return None

    def set(self, name, value):
        self.set_single_row('name="%s"' % name, 
                name=name, value=value)


class BORSqlite3File(BORFile):

    __db__       = None
    __filename__ = 'data.db'
    __tables__   = (('prop', 
                ('name', 'CHAR(100)'), 
                ('value', 'TEXT')))

    def __init__(self, *args, **kwargs):
        super(BORSqlite3File, self).__init__(*args, **kwargs)
        self.__tables__ = list()
        for ann in dir(self):
            att = getattr(self, ann)
            if isinstance(att, SQLTable):
                att.__interface__ = self
                self.__tables__.append(att)

    def ensure_load(self):
        pass

    def __open__(self, mode=None):
        self.__db__ = sqlite3.connect(self.filepath)
    
    def __close__(self):
        if self.changed:
            self.flush()
        self.__db__.close()
        self.__db__ = None
    
    def __enter__(self):
        return super(BORSqlite3File, self).__enter__()

    def __exit__(self, exception_type=None, 
                exception_value=None, traceback=None):
        return super(BORSqlite3File, self).__exit__(exception_type=exception_type,
                exception_value=exception_value, traceback=traceback)

    def query(self, query):
        ''' * query: string, list of strings '''
        if isinstance(query, str):
            query = (query,)
        self.lock()
        c = self.__db__.cursor()
        for q in query:
            log.debug('SQL: %s' % q)
            c.execute(q)
        self.set_changed()
        self.unlock()
        return c

    def flush(self):
        ''' Flush data to the file ''' 
        self.__db__.commit()
        self.set_changed(False)

    def create(self):
        super(BORSqlite3File, self).create()
        self.lock()
        log.debug('Create database: %s' % self.filepath)
        for table in self.__tables__:
            table.create()
        self.unlock()
        return self

