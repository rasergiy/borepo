'''
Binary Object Repository Xml 
'''
import re
import functools
from xml.sax.saxutils import escape, unescape
from xml.etree import ElementTree as ET
from xml.dom.minidom import parseString
from borepo.const import *
import borepo.errors as errors
from borepo.file import BORFile

NODE_NAME_REGEXP = r'^[-=+_ ./a-zA-Z0-9]+$'
ELEM_NAME_REGEXP = r'^[-=+_ ./a-zA-Z0-9]+$'

__all__ = [
        'BORXmlFile', 
        'nodelist2xml']

def nodelist2xml(xml, nodes):
    '''
    replicate python list of hash items to xml
    hash keys: 
        name - node name
        [attr] - dict of attributes
        [nodes] - list of child nodes, that will be recursively parsed
    '''
    for node in nodes:
        if isinstance(node, str):
            name, cls = node, str
        else:
            name, cls = node
        el = ET.Element(name, count='0')
        #if len(nod):
        #    nodelist2xml(el, node['nodes'])
        #if 'value' in node:
        #    el.text = node['value']
        xml.append(el)


class BORXmlFile(BORFile):
    
    ''' 
    Binary Object Repository Xml object
    '''

    __xml__ = None
    __filename__ = 'index.xml'
    __root__ = ('index', {'version': VERSION})
    __root_nodes__ = ()
    
    __pretty_xml__ = False
    __pretty_xml_indent__ = '  '

    def __init__(self, **kwargs):
        self.__pretty_xml__ = kwargs.pop('pretty_xml', 
                self.__pretty_xml__)
        self.__pretty_xml_indent__ = kwargs.pop('pretty_xml_indent', 
                self.__pretty_xml_indent__)
        self.__root_nodes__ = kwargs.pop('root_nodes', 
                self.__root_nodes__)
        def strgetter(self, node_name, elem_name=None):
            return self.get_node_val(elem_name, node_name)
        def boolgetter(self, node_name, elem_name=None):
            return self.get_node_bool(elem_name, node_name)
        def valsetter(self, node_name, value='', elem_name=None):
            return self.set_node(elem_name, node_name, value)
        def countgetter(self, elem_name=None):
            return self.get_element_count(elem_name)
        def countsetter(self, value, elem_name=None):
            return self.set_element_count(elem_name, value)
        for node in self.__root_nodes__:
            if isinstance(node, str):
                name, cls = node, str
            else:
                name, cls = node[0], node[1]
            if cls is bool:
                getfunc = boolgetter
            else:
                getfunc = strgetter

            def lset(node, nodename, name, func, *args, **kwargs):
                kwargs['elem_name'] = nodename
                if getattr(self, name,None) is None:
                    setattr(self, name, functools.partial(
                        func, self, *args, **kwargs))
            lset(node, name, 'get_%s' % name, getfunc)
            lset(node, name, 'set_%s' % name, valsetter)
            lset(node, name, 'get_%s_count' % name, countgetter)
            lset(node, name, 'set_%s_count' % name, countsetter)
        super(BORXmlFile, self).__init__(**kwargs)
    
    def __enter__(self):
        return super(BORXmlFile, self).__enter__()

    def __exit__(self, exception_type=None, 
                exception_value=None, traceback=None):
        return super(BORXmlFile, self).__exit__(exception_type=exception_type,
                exception_value=exception_value, traceback=traceback)

    def create(self):
        super(BORXmlFile, self).create()
        root_name, root_attributes = self.__root__
        root = ET.Element(root_name,
                **root_attributes)
        nodelist2xml(root,  self.__root_nodes__)
        self.__xml__ = root
        self.set_changed()
        return self

    def read(self):
        self.lock()
        self.__xml__ = ET.ElementTree().parse(self.filepath)
        self.set_changed(False)
        self.unlock()
    
    def flush(self):
        self.lock()
        tree = ET.ElementTree(self.__xml__)
        if self.__pretty_xml__:
            rough_string = ET.tostring(self.__xml__, 'utf-8')
            reparsed = parseString(rough_string)
            with open(self.filepath, 'w') as f:
                f.write(reparsed.toprettyxml(indent=self.__pretty_xml_indent__))
        else:
            tree.write(self.filepath)
        self.set_changed(False)
        self.unlock()

    def ensure_load(self):
        if self.__xml__ is None:
            self.read()

    def get_node(self, elem_name, node_name):
        ''' -> Element's Node '''
        if not re.match(NODE_NAME_REGEXP, str(node_name)):
            raise errors.BORXmlElementNameError(elem_name)
        self.lock()
        self.ensure_load()
        node = self.get_element(elem_name).find("./node[@name='%s']" % node_name)
        self.unlock()
        return node
    
    def get_node_val(self, elem_name, node_name, strip=True):
        ''' -> Element's Node's value '''
        node = self.get_node(elem_name, node_name)
        if node is None:
            return None
        if strip:
            return unescape(str(node.text)).strip()
        else:
            return unescape(str(node.text))
    
    def get_node_bool(self, elem_name, node_name):
        ''' -> bool, Node existance '''
        if self.get_node(elem_name, node_name) is None:
            return False
        return True
    
    def set_node(self, elem_name, node_name, value=''):
        '''
        Set ./<elem>/node[@name] with value, setup ./<elem>[@count]
        if value is None - removes <elem> element
        '''
        if not re.match(NODE_NAME_REGEXP, str(node_name)):
            raise errors.BORXmlElementNameError(elem_name)
        self.lock()
        if value is None:
            self.remove_node(elem_name, node_name)
        else:
            elem = self.get_element(elem_name)
            node = self.get_node(elem_name, node_name)
            if node is None:
                node = ET.Element('node', name=str(node_name))
                elem.append(node)
                self.__inc_node_count__(elem)
            node.text = escape(str(value))
            self.set_changed()
        self.unlock()

    def remove_node(self, elem_name, node_name):
        self.lock()
        elem = self.get_element(elem_name)
        node = self.get_node(elem_name, node_name)
        if not node is None:
            elem.remove(node)
            self.__inc_node_count__(elem, -1)
        self.set_changed()
        self.unlock()
    
    def remove_element(self, elem_name):
        self.lock()
        elem = self.get_element(elem_name)
        self.__xml__.remove(elem)
        self.set_changed()
        self.unlock()
    
    def get_element(self, elem_name):
        ''' -> Element. raises BORXmlElementNotFoundError if not found '''
        self.lock()
        self.ensure_load()
        elem = self.__xml__.find(str(elem_name))
        if elem is None:
            raise errors.BORXmlElementNotFoundError(elem_name)
        self.unlock()
        return elem
    
    def set_element(self, elem_name, elem_value):
        self.lock()
        self.ensure_load()
        elem = self.get_element(elem_name)
        elem.text = elem_value
        self.set_changed()
        self.unlock()
    
    def add_element(self, elem_name, value='', **attributes):
        '''
        Set ./<elem>/node[@name] with value, setup ./<elem>[@count]
        if value is None - removes <elem> element
        '''
        if not re.match(ELEM_NAME_REGEXP, str(elem_name)):
            raise errors.BORXmlElementNameError(elem_name)
        self.lock()
        elem = ET.Element(elem_name, **attributes)
        elem.text = escape(str(value))
        self.__xml__.append(elem)
        self.set_changed()
        self.unlock()

    def get_element_count(self, elem_name):
        self.ensure_load()
        return int(self.get_element(elem_name).get('count'))
    
    def set_element_count(self, elem_name, val):
        self.ensure_load()
        self.get_element(elem_name).set('count', str(val))

    def get_version(self):
        self.ensure_load()
        return self.__xml__.get('version')
    
    def __inc_node_count__(self, elem, incval=1):
        e = elem.get('count')
        if e is None:
            return e
        val = int(e)+incval
        elem.set('count', str(val))
        return val

    version  = property(get_version)
