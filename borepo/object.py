'''
BinaryObjectRepository Object
open file nor
'''

import io
import os
import re
import hashlib
import datetime
from borepo.const import *
from borepo.file import ChunkedFile
from borepo.binfile import BORBinFile
from borepo.objectindex import BORObjectIndex

CACHE_NAME_REGEXP = '^[-_0-9a-zA-Z.]+$'
__all__ = ['BORObject']

class BORObject(BORBinFile):

    __index__ = None
    __xml_index__ = None
    __filename__ = 'object.data'
    __cache_dir__ = 'cache'
    __links_dir__ = 'file'
    
    def __init__(self, **kwargs):
        super(BORObject, self).__init__(**kwargs)
        self.__index__ = BORObjectIndex(
                basepath = self.basepath)

    def __enter__(self):
        self.__index__.__enter__()
        return super(BORObject, self).__enter__()

    def __exit__(self, exception_type=None, 
                exception_value=None, traceback=None):
        super(BORObject, self).__exit__(exception_type=exception_type,
                exception_value=exception_value, traceback=traceback)
        self.__index__.__exit__(exception_type=exception_type,
                exception_value=exception_value, traceback=traceback)
    
    def update_hashes(self):
        ''' update hashes '''
        for alg in HASH_ALGORITHMS:
            self.__index__.set_hash(alg, 
                    super(BORObject, self).get_hash(alg))

    def lock(self, mode=None, timeout=None):
        ''' This lock is without file opening '''
        return super(BORObject, self).lock(mode=mode, 
                timeout=timeout)

    def __on_data_calc_hash__(self, data):
        for hash in self.hashes.values():
            hash.update(data)
    
    def create(self, data_src=bytes()):
        ''' create object from data (bytes, io.IOBase, ChunkedFile) '''
        self.hashes = {alg:hashlib.new(alg) for alg in HASH_ALGORITHMS}
        super(BORObject, self).create(data_src=data_src, on_data = self.__on_data_calc_hash__)
        with self.__index__.create():
            for alg, hash in self.hashes.items():
                self.__index__.set_hash(alg, hash)
        os.makedirs(os.path.join(self.basepath, 
                self.__links_dir__))
        return self
    
    def delete(self):
        super(BORObject, self).delete()
        self.__index__.delete()

    def get_hash(self, alg):
        return str(self.__index__.get_hash(alg).text).strip()

    def get_cache(self, name):
        return self.__index__.get_cache(name)
    
    def get_cache_path(self, name, check_existance=True):
        cachedir = os.path.join(self.basepath, 
                self.__cache_dir__)
        cachepath = os.path.join(cachedir, name)
        if not check_existance:
            return cachepath
        cache = self.__index__.get_cache(name)
        if cache is None or not os.path.exists(cachepath):
            return None
        return cachepath

    def delete_cache(self, name):
        path = self.get_cache_path(name)
        if path:
            os.remove(path)
            self.__index__.set_cache(name, None)

    def set_cache(self, name, data_src):
        ''' data_src - string and bytes instances will be written
        , or instance accepted by ChunkedFile.get '''
        if not re.match(CACHE_NAME_REGEXP, name):
            raise errors.BORNameError(name)
        cachepath = self.get_cache_path(name, check_existance=False)
        cachedir = os.path.dirname(cachepath)
        if not os.path.exists(cachedir):
            os.makedirs(cachedir)
        if isinstance(data_src, bytes):
            if data_src:
                with open(cachepath, mode='wb+') as f:
                    f.write(data_src)
        elif isinstance(data_src, str):
            if data_src:
                with open(cachepath, mode='w+') as f:
                    f.write(data_src)
        else:
            with ChunkedFile.get(data_src, 'rb') as src:
                with ChunkedFile.get(cachepath, 'wb+') as dst:
                    for chunk in src.chunks():
                        dst.write(chunk)
        return self.__index__.set_cache(name, 
                datetime.datetime.utcnow().isoformat())

    def get_meta(self, name):
        return self.__index__.get_meta(name)
    
    def set_meta(self, name, value):
        return self.__index__.set_meta(name, value)
    
    def get_link(self, name):
        return self.__index__.get_link(name)
    
    def set_link(self, name, value):
        if value is None:
            os.remove(os.path.join(self.basepath, self.__links_dir__, 
                self.__index__.get_link(name)))
        else:
            os.symlink(
                    os.path.join('..', self.__filename__), 
                    os.path.join(self.basepath, self.__links_dir__, value))
        return self.__index__.set_link(name, value)

    def get_cache_count(self):
        ''' -> int '''
        return self.__index__.cache_count

    def get_meta_count(self):
        ''' -> int '''
        return self.__index__.meta_count
    
    def get_link_count(self):
        ''' -> int '''
        return self.__index__.link_count

    def move(self, newlocation):
        os.makedirs(os.path.join(path, obj.__links_dir__))
        os.makedirs(os.path.join(path, obj.__cache_dir__))
        os.rename(obj.filepath, t.filepath)
        os.rename(obj.__index__.filepath, t.__index__.filepath)
        shutil.rmtree(obj.basepath)
        self.__index_file__.lock()
        self.__index_file__.add_object(t)
        self.__index_file__.unlock()
    
    link_count  = property(get_link_count)
    meta_count  = property(get_meta_count)
    cache_count = property(get_cache_count)


